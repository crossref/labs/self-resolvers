import datetime
import json
import ijson
import logging
from pathlib import Path
import pandas as pd


import typer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def all_members():
    with open("members.json", "rb") as f:
        yield from ijson.items(f, "message.items.item")


def sense_check(member):
    try:
        return (
            member["counts"]["total-dois"] > 0
            and member["cr-labs-member-profile"]["account-type"] != "Uncategorized"
        )
    except KeyError as e:
        # logging.error(f"Error checking sanity of {member['id']}: {e}\n {member}")
        logging.error(f"Error sense checking {member['id']}")

        return False


def get_member_domains(member_id):
    try:
        with open(f"tlds/{member_id}.json", "rb") as f:
            return json.load(f).keys()
    except Exception as e:
        logger.error(f"Error loading tlds for {member_id}: {e}")
        return []


# def own_domain(domain, member_id):
#     member_domains = get_member_domains(member_id)
#     return domain in member_domains


def self_resolution(resolutions, member_id):
    self_res_count = 0
    total_res_count = 0
    self_res_domains = set()
    member_domains = get_member_domains(member_id)
    for log_entry in resolutions:
        # total_res_count += log_entry["total-count"]
        try:
            for doi in log_entry["breakdowns"]["publisher-domain"]:
                domain = doi["value"]
                count = doi["count"]
                total_res_count += count
                if domain in member_domains:
                    logger.info(f"{member_id} self-resolved: {domain}")
                    self_res_count += count
                    self_res_domains.add(domain)

        except KeyError:
            logger.warning(f"No publisher-domain breakdown for {member_id}")
            pass

    self_res_ratio = 0 if total_res_count == 0 else self_res_count / total_res_count

    if self_res_ratio > 0:
        logger.warning(f"{member_id} self-resolves {self_res_ratio}")
    return {
        "self_res_count": self_res_count,
        "total_res_count": total_res_count,
        "self_res_ratio": self_res_ratio,
        "member_domains": "\n".join(member_domains),
        "self_res_domains": "\n".join(self_res_domains),
    }


def summarize(member):
    logging.debug(f"Summarizing {member['id']}")

    header = {
        "member_id": member["id"],
        "member_name": member["primary-name"],
        "member_type": member["cr-labs-member-profile"]["account-type"],
        "date_joined": member["cr-labs-member-profile"]["date-joined"],
        "total_dois": member["counts"]["total-dois"],
    }
    return (
        header
        | self_resolution(member["cr-labs-resolution"], member["id"])
        | {
            "api_info": f"https://api.labs.crossref.org/works/{member['id']}?mailto=labs@crossref.org"
        }
    )


def main(
    verbose: bool = typer.Option(False, "--verbose", "-v"),
):
    """A simple CLI for testing typer"""
    if verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    typer.echo(f"Verbose: {verbose}")

    try:
        logger.info("Starting self-resolvers")
        summaries = [
            summarize(member) for member in all_members() if sense_check(member)
        ]
        logger.info("Finished self-resolvers")
        df = pd.DataFrame(summaries)
        df.to_csv("self-resolvers.csv", index=False)

    except Exception as e:
        typer.echo(f"Error: {e}")
        raise typer.Exit(code=1) from e


if __name__ == "__main__":
    typer.run(main)
