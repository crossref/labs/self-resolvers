# Self-Resolution Report

We apparently have members who deposit ["Sneaked References."](https://arxiv.org/abs/2310.02192) That is, they register references in their Crossref metadata that do not appear in the text of the article itself. The theory is that they do this in order to boost the citations counts of other papers that they publish.

Hypothesis: Members who deliberately register "sneaked references" in their metadata with show an unusually high "self-resolution ratio" based on their resolution reports.

Can we estimate what percentage of our members resolutions come from their own domains?

We can. Sort of.

- We can calculate a list of all the top-level domains (TLDs) that a member points their DOIs at. We can do this by looking at a representative sample of the member's works.
- The union of these individual publisher TLD lists represents a list of "publisher domains."
- Every month Crossref gets a list of DOI resolution logs. We summarize these logs for our members and we currently expose them in the Crossref labs API.

For example, see the `cr-labs-resolutions-key` for the following API response: https://api.labs.crossref.org/members/272?mailto=labs@crossref.org` . Under this section, you can see breakdowns, one of which is a breakdown of the top 10 publisher domains that resolve to the member in question.

Using the list of member domains, we can see what percentage of the top referring domains belong to the member- in other words, the "self resolution ratio."

There are some limitations to the technique. Some Crossref members host on shared hosting platforms and they share a common domain (e.g. `ingenta.com`). In these cases it is impossible to tell if a resolution to `ingenta.com --> ingenta.com` is coming from the same member.

But if a member has their own domain, then the technique should show self-resolutions.

This repo contains a script (`self-resolvers.py`) that iterates over a json dump of our members data (`members.json`) from the Crossref Labs REST API.

It looks at the publisher domains breakdown and compares each domain to a list of publisher-owned domains. For each member it generates a record that contains the following elements:

- `member_id`
- `member_name`
- `member_type`
- `date_joined`
- `total-dois`
- `self-res-count` (The count of resolutions that come from a member's domain)
- `total-res-count` (Count of the total resolutions that come from publisher domains)
- `self-res-ratio` (self_res_count / total_res_count)
- `member domains` (domains the member hosts DOIs on)
- `self-res-domains` (member owned domains that resolved to member's DOIs)
- `api_info` (a link to the Crossref Labs REST API entry for the member)

Looking at the `self-resolvers.csv` file in a spreadsheet and you can sort on the `self-res-ratio` and filter members that have a relatively high number of total-dois.




